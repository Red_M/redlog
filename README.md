RedLog
=======

RedLog is a syslog server that is meant to notify administrators about events.

RedLog's main purpose is to provide a highly available syslog server that can notify administrators about network or system events.
A small amount of client verification is done about the client.
For the moment this is only a stdout syslog server, notifications will be proived later.


-Red_M

Requirements:
=============
- Python 2.7+
- Watchdog

Installing:
==========
1. Install Python 2.7 or higher

2. Install requirements (pip install -r ./requirements.txt)

3. git clone https://bitbucket.org/Red_M/redlog.git


Running:
========
1. Run ./redlog.py

2. Configure the settings via ./config.json

3. Enjoy
