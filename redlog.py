#!/usr/bin/env python2
# RedServ
# Copyright (C) 2016  Red_M ( http://bitbucket.com/Red_M )

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import socket
import threading
import SocketServer
import argparse
import time
import os,sys
import json
import re
import traceback
import cgi
from watchdog.observers import Observer as watchdog_observer
from watchdog.events import FileSystemEventHandler as watchdog_file_event_handler


os.chdir('.' or sys.path[0])
current_dir = os.path.join(os.getcwd(),os.sep.join(sys.argv[0].split(os.sep)[0:-1]))
if current_dir.endswith("."):
    current_dir = current_dir[0:-1]
if sys.argv[0].split(os.sep)[-1] in os.listdir(current_dir):
    print("INFO: Found syslog server path")
else:
    print("INFO: Bad syslog server path")
    exit()

config_cache = []
regexes = {
    'hostname':re.compile(r'hostname:(.+?):'),
    'openwrt':re.compile(r'^<\d+>(?P<time_reported>.+?) hostname:(?P<hostname>.+?): (?P<service>.+?) (?P<process>.+?)(?P<pid>\[(.+?)\]:|:) (?P<message>.+?)\x00')
}
generic_line = '<hostname>: <service> <process><pid> <message>'

class RedServer(object):

    def trace_back(self,html=False):
        type_, value_, traceback_ = sys.exc_info()
        ex = traceback.format_exception(type_, value_, traceback_)
        trace = ""
        for data in ex:
            trace = str(trace+data)
        trace = cgi.escape(trace).encode('utf-8', 'xmlcharrefreplace')
        if html==True:
            trace = trace.replace("\n","<br>")
        return(trace)
    
    def debugger(self,lvl=5,message=""):
        message = str(message)
        levels = {
            0:'FATAL',
            1:'CRITICAL',
            2:'ERROR',
            3:'INFO',
            4:'MESSAGE',
            5:'DEBUG'
        }
        if lvl in levels:
            lvl = levels[lvl]
        if "\n" in message:
            message = message.replace("\n","\n"+str(lvl)+": ")
        print(str(lvl)+": "+message)
    
    def sysinfo(self):
        if os.name=="posix":
            (sysname, nodename, release, version, machine) = os.uname()
        else:
            (nodename, v4, v6) = socket.gethostbyaddr(socket.gethostname())
        return(nodename)

def get_config_default():
    config_file_data = {
        'hosts':{
            'test':{
                'ip':'127.0.0.1',
                'device_type':'test device'
            }
        },
        'groups':{
            'test':'test_group'
        },
        'notifications':{
            'test_group':'email_test'
        }
    }
    return(config_file_data)

def config_init(config_location):
    if not os.path.exists(config_location):
        config_file_data = get_config_default()
        open(config_location, 'w').write(json.dumps(config_file_data, sort_keys=True,indent=2, separators=(',', ': ')))

def load_config(config_location):
    try:
        if os.path.getmtime(config_location)>config_cache[1]:
            config_cache[0] = json.load(open(config_location))
            config_cache[1] = os.path.getmtime(config_location)
        return(config_cache[0])
    except ValueError, e:
        RedServ.debugger(0,'malformed config! '+e)

class ConfigFileEventHandler(object):

    def on_any_event(self, event):
        """Catch-all event handler.

        :param event:
            The event object representing the file system event.
        :type event:
            :class:`FileSystemEvent`
        """

    def on_moved(self, event):
        pass

    def on_created(self, event):
        global conf
        what = 'directory' if event.is_directory else 'file'
        if what=='file':
            if event.src_path.split(os.sep)[-1]=="config.json":
                if not os.stat(event.src_path).st_size==0:
                    conf = load_config(event.src_path)

    def on_deleted(self, event):
        pass

    def on_modified(self, event):
        global conf
        what = 'directory' if event.is_directory else 'file'
        if what=='file':
            if event.src_path.split(os.sep)[-1]=="config.json":
                if not os.stat(event.src_path).st_size==0:
                    conf = load_config(event.src_path)

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

class ThreadedUDPServer(SocketServer.ThreadingMixIn, SocketServer.UDPServer):
    pass


class ThreadedRequestHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        global RedServ
        global config
        global regexes
        try:
            if isinstance(self.request,type(('',''))):
                raw_line = self.request[0]
            else:
                raw_line = self.request.recv(2048)
            # RedServ.debugger(3,raw_line)
        
            hostname = re.search(regexes['hostname'],raw_line).group(1)
            (client_ip, client_port) = self.client_address
            
            listen_to_request = True
            
            if not hostname in config['hosts']:
                #bad, drop line
                listen_to_request = False
            if not client_ip==config['hosts'][hostname]['ip']:
                #bad, drop line
                listen_to_request = False
            assert listen_to_request
            if config['hosts'][hostname]['device_type']=='openwrt':
                line_data = {
                    'time_reported':'',
                    'hostname':'',
                    'service':'',
                    'process':'',
                    'pid':'',
                    'message':''
                }
                match = re.match(regexes['openwrt'],raw_line)
                for line_match in line_data:
                    line_data[line_match] = match.group(line_match)
                # RedServ.debugger(3,line_data)
                out_line = ''
                line_template = generic_line
                for replacement in line_data:
                    line_template = line_template.replace('<'+replacement+'>',line_data[replacement])
                out_line = line_template
                RedServ.debugger(3,out_line)
        except Exception as e:
            RedServ.debugger(2,raw_line)
            RedServ.debugger(2,RedServ.trace_back())


def init_network_protocol(server):
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.daemon = True
    server_thread.start()
    return(server_thread)

def main(PORT):
    global RedServ
    global config
    os.chdir(current_dir)
    
    conflocation = os.path.join(current_dir,"config.json")
    config_init(conflocation)
    config_cache.append(json.load(open(conflocation)))
    config_cache.append(os.path.getmtime(conflocation))
    config = load_config(conflocation)
    
    config_file_event_handler = ConfigFileEventHandler()
    config_event_handler = watchdog_file_event_handler()
    config_event_handler.on_any_event = config_file_event_handler.on_any_event
    config_event_handler.on_moved = config_file_event_handler.on_moved
    config_event_handler.on_created = config_file_event_handler.on_created
    config_event_handler.on_deleted = config_file_event_handler.on_deleted
    config_event_handler.on_modified = config_file_event_handler.on_modified
    config_observer = watchdog_observer()
    config_observer.schedule(config_event_handler, current_dir, recursive=False)
    config_observer.start()
    
    HOST = "0.0.0.0"

    tcp_server = ThreadedTCPServer((HOST, PORT), ThreadedRequestHandler)
    udp_server = ThreadedUDPServer((HOST, PORT), ThreadedRequestHandler)
    
    tcp_server_thread = init_network_protocol(tcp_server)
    udp_server_thread = init_network_protocol(udp_server)
    while True:
        try:
            time.sleep(0.1)
        except Exception as e:
            tcp_server_thread.join()
            udp_server_thread.join()
            print(RedServ.trace_back())
    config_observer.stop()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='RedServ SysLog server, RedLog.')
    parser.add_argument('--port', help='Port for servers', default=514, required=False)
    args = parser.parse_args()
    RedServ = RedServer()
    config = ''
    main(int(args.port))
